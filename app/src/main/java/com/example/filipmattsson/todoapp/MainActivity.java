package com.example.filipmattsson.todoapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends Activity {

    private static final int viewReqId = 746;
    private static final int deleteReqId = 156;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void createButtonClicked (View view) {
        Intent intent = new Intent(this, CreateActivity.class);
        startActivity(intent);
    }

    public void viewButtonClicked (View view) {
        Intent intent = new Intent(this, PickActivity.class);
        startActivityForResult(intent, viewReqId);
    }

    public void deleteButtonClicked (View view) {
        Intent intent = new Intent(this, PickActivity.class);
        startActivityForResult(intent, deleteReqId);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == viewReqId) {
            if(resultCode == Activity.RESULT_OK) {
                data.setClass(this, DetailActivity.class);
                startActivity(data);
            }
        }
        else if(requestCode == deleteReqId) {
            if(resultCode == Activity.RESULT_OK) {
                data.setClass(this, DeleteActivity.class);
                startActivity(data);
            }
        }
    }
}
