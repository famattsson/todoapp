package com.example.filipmattsson.todoapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

public class DetailActivity extends Activity {
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Intent intent = getIntent();
        int todoIndex = intent.getIntExtra("todoIndex", -1);
        Data.ToDo toDo = Data.todos.get(todoIndex);
        TextView todoTitle = (TextView) findViewById(R.id.TodoTitle);
        todoTitle.setText(toDo.title);
    }
}
