package com.example.filipmattsson.todoapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

public class DeleteActivity extends Activity {
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete);
        Intent intent = getIntent();
        int todoIndex = intent.getIntExtra("todoIndex", -1);
        Data.ToDo toDo = Data.todos.get(todoIndex);
        TextView todoTitle = (TextView) findViewById(R.id.TodoTitle);
        todoTitle.setText(toDo.title);
    }

    public void onDeleteConfirmClick (View view) {
        new AlertDialog.Builder(this)
            .setTitle("Delete ToDo")
            .setMessage("Do you really want to delete it?")
            .setPositiveButton(
                android.R.string.yes,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Data.todos.remove(getIntent().getIntExtra("todoIndex", -1));
                        finish();
                    }
                }
            ).setNegativeButton(
                android.R.string.no,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }
            ).show();
    }
}
